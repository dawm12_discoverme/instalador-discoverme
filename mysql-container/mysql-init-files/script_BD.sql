﻿DROP DATABASE hotel_developer;
CREATE DATABASE hotel_developer;
USE hotel_developer;

CREATE TABLE `perfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(500) NOT NULL,
  `disponible` bit(1) DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `nombre` varchar(75) NOT NULL UNIQUE,
  `precio` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) NOT NULL UNIQUE,
  `logo` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `usuarios` (
  `id` varchar(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `passwd` varchar(10) NOT NULL,
  `procedencia` varchar(50) DEFAULT NULL,
  `foto_id` int(11) DEFAULT NULL,
  `perfil_id` int(11) DEFAULT NULL,
  `rol_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk2t7jybieslpvde0l0d91lr07` (`foto_id`),
  KEY `FKq3a59nsli56gq634i3235693d` (`perfil_id`),
  KEY `FKqf5elo4jcq7qrt83oi0qmenjo` (`rol_id`),
  CONSTRAINT `FKk2t7jybieslpvde0l0d91lr07` FOREIGN KEY (`foto_id`) REFERENCES `fotos` (`id`),
  CONSTRAINT `FKq3a59nsli56gq634i3235693d` FOREIGN KEY (`perfil_id`) REFERENCES `perfiles` (`id`),
  CONSTRAINT `FKqf5elo4jcq7qrt83oi0qmenjo` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `experiencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` longtext NOT NULL,
  `fecha_fin` date DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `mapa` longtext,
  `mins_distancia` int(11) DEFAULT NULL,
  `nombre` varchar(75) NOT NULL UNIQUE,
  `precio` int(11) DEFAULT '0',
  `puntuacion` int(11) NOT NULL DEFAULT '0',
  `usuario_id` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnm6h1xep7e5g0idu1k59cpmaa` (`usuario_id`),
  CONSTRAINT `FKnm6h1xep7e5g0idu1k59cpmaa` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comentario` varchar(300) NOT NULL,
  `fecha` date NOT NULL,
  `puntos` int(11) NOT NULL,
  `titulo` varchar(75) NOT NULL,
  `experiencia_id` int(11) NOT NULL,
  `usuario_id` varchar(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtj53pcwxn7iw5a67g7j4kh7h` (`experiencia_id`),
  KEY `FKdts62yj83qe3k748cgcjvm48r` (`usuario_id`),
  CONSTRAINT `FKdts62yj83qe3k748cgcjvm48r` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `FKtj53pcwxn7iw5a67g7j4kh7h` FOREIGN KEY (`experiencia_id`) REFERENCES `experiencias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `ofertas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) NOT NULL UNIQUE,
  `nombre` varchar(75) NOT NULL UNIQUE,
  `descripcion` varchar(500) NOT NULL,
  `fecha_fin` date DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `experiencia_id` int(11),
  PRIMARY KEY (`id`),
  KEY `FKpw1ng442j393mpvto0d7w1gyl` (`experiencia_id`),
  CONSTRAINT `FKpw1ng442j393mpvto0d7w1gyl` FOREIGN KEY (`experiencia_id`) REFERENCES `experiencias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `servicios_fotos` (
  `servicio_id` int(11) NOT NULL,
  `fotos_id` int(11) NOT NULL,
  PRIMARY KEY (`servicio_id`,`fotos_id`),
  UNIQUE KEY `UK_lgei46a7adudlfusdw0ib7pna` (`fotos_id`),
  CONSTRAINT `FKec23jvpa38egmowufwde1xsi8` FOREIGN KEY (`servicio_id`) REFERENCES `servicios` (`id`),
  CONSTRAINT `FKiov5l35purr6qv8hf60u2jh73` FOREIGN KEY (`fotos_id`) REFERENCES `fotos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `experiencias_fotos` (
  `experiencia_id` int(11) NOT NULL,
  `fotos_id` int(11) NOT NULL,
  PRIMARY KEY (`experiencia_id`,`fotos_id`),
  UNIQUE KEY `UK_93tk084qxl2bg44nmolvooktm` (`fotos_id`),
  CONSTRAINT `FKb89ictkl8oj40kw7qiy2f3jx9` FOREIGN KEY (`fotos_id`) REFERENCES `fotos` (`id`),
  CONSTRAINT `FKgums18aac84w94kpto49c20p5` FOREIGN KEY (`experiencia_id`) REFERENCES `experiencias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `experiencias_tipos` (
  `experiencia_id` int(11) NOT NULL,
  `tipos_id` int(11) NOT NULL,
  PRIMARY KEY (`experiencia_id`,`tipos_id`),
  CONSTRAINT `FKhm60v8q2ox9jhqo3e2vphipip` FOREIGN KEY (`tipos_id`) REFERENCES `tipos` (`id`),
  CONSTRAINT `FKtph3344angs1sy9p768ujqjic` FOREIGN KEY (`experiencia_id`) REFERENCES `experiencias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `perfiles_tipos` (
  `perfil_id` int(11) NOT NULL,
  `tipos_id` int(11) NOT NULL,
  PRIMARY KEY (`perfil_id`,`tipos_id`),
  KEY `FKe6rijsgj4ilqtm21ukelqpkb2` (`tipos_id`),
  CONSTRAINT `FKd4eea9hhpmxvddodt5y0m2qji` FOREIGN KEY (`perfil_id`) REFERENCES `perfiles` (`id`),
  CONSTRAINT `FKe6rijsgj4ilqtm21ukelqpkb2` FOREIGN KEY (`tipos_id`) REFERENCES `tipos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TRIGGER update_puntuacion_insert_comentario AFTER INSERT ON comentarios
FOR EACH ROW UPDATE experiencias SET puntuacion = (SELECT avg(puntos) FROM comentarios WHERE experiencia_id = NEW.experiencia_id) WHERE id = NEW.experiencia_id;

CREATE TRIGGER update_puntuacion_update_comentario AFTER UPDATE ON comentarios
FOR EACH ROW UPDATE experiencias SET puntuacion = (SELECT avg(puntos) FROM comentarios WHERE experiencia_id = NEW.experiencia_id) WHERE id = NEW.experiencia_id;

CREATE TRIGGER update_puntuacion_delete_comentario AFTER DELETE ON comentarios
FOR EACH ROW UPDATE experiencias SET puntuacion = (SELECT avg(puntos) FROM comentarios WHERE experiencia_id = OLD.experiencia_id) WHERE id = OLD.experiencia_id;

INSERT INTO perfiles (nombre) VALUES ("Deluxe");
INSERT INTO perfiles (nombre) VALUES ("Soltero");
INSERT INTO perfiles (nombre) VALUES ("Low Cost");
INSERT INTO perfiles (nombre) VALUES ("Aventurero");
INSERT INTO perfiles (nombre) VALUES ("Familiar");
INSERT INTO perfiles (nombre) VALUES ("Mayor de 50");

INSERT INTO roles (nombre) VALUES ("Administrador");
INSERT INTO roles (nombre) VALUES ("Recepcionista");
INSERT INTO roles (nombre) VALUES ("Camarero");
INSERT INTO roles (nombre) VALUES ("Huesped");
INSERT INTO roles (nombre) VALUES ("Colaborador");

INSERT INTO servicios (nombre,descripcion,disponible,fecha_inicio,fecha_fin,hora_inicio,hora_fin)
VALUES ("Piscina exterior","Disfruta tomando el sol y refrescándote en nuestra piscina exterior.",true,'2019-05-01','2019-10-30','08:00:00','20:00:00');
INSERT INTO servicios (nombre,descripcion,disponible,hora_inicio,hora_fin)
VALUES ("Servicio SPA","Disfruta relajándote en nuestro SPA.",true,'08:00:00','20:00:00');
INSERT INTO servicios (nombre,descripcion,disponible,hora_inicio,hora_fin,precio)
VALUES ("Servicio desayunos","Disfruta del buffet continental de desayuno.",false,'08:00:00','11:00:00',10);
INSERT INTO servicios (nombre,descripcion,disponible,hora_inicio,hora_fin,precio)
VALUES ("Servicio comidas","Disfruta del buffet continental de comidas.",true,'13:00:00','15:00:00',20);
INSERT INTO servicios (nombre,descripcion,disponible,hora_inicio,hora_fin,precio)
VALUES ("Servicio cenas","Disfruta del buffet continental de cenas.",false,'20:00:00','23:00:00',20);

INSERT INTO tipos (nombre) VALUES ("Cultural");
INSERT INTO tipos (nombre) VALUES ("Natural");
INSERT INTO tipos (nombre) VALUES ("Deportivo");
INSERT INTO tipos (nombre) VALUES ("Eventos");
INSERT INTO tipos (nombre) VALUES ("Folclore");
INSERT INTO tipos (nombre) VALUES ("Nocturno");

INSERT INTO fotos (foto,logo) VALUES ("Avatar-01.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-02.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-03.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-04.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-05.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-06.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-07.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-08.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-09.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Avatar-10.jpg",false);

INSERT INTO fotos (foto,logo) VALUES ("Museo-de-cera-0.png",true);
INSERT INTO fotos (foto,logo) VALUES ("Museo-de-cera-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Museo-de-cera-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Museo-de-cera-3.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("L'ovella-negra-0.png",true);
INSERT INTO fotos (foto,logo) VALUES ("L'ovella-negra-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("L'ovella-negra-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("L'ovella-negra-3.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Calsotada-popular-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Calsotada-popular-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Calsotada-popular-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Calsotada-popular-3.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Cami-de-les-aigues-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Cami-de-les-aigues-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Cami-de-les-aigues-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Cami-de-les-aigues-3.jpg",false);

INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id,perfil_id) VALUES ("00000000X",4,"Manuel","12345","spain",4,3);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id,perfil_id) VALUES ("11111111X",5,"Carlos","12345","spain",4,5);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id,perfil_id) VALUES ("22222222X",6,"Jose","12345","spain",4,2);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id) VALUES ("33333333X",8,"David","12345","spain",2);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id) VALUES ("44444444X",1,"Noemi","12345","spain",3);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id) VALUES ("55555555X",2,"Laura","12345","spain",3);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id) VALUES ("66666666X",3,"Maria","12345","spain",2);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id) VALUES ("77777777X",9,"Javier","12345","spain",1);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id) VALUES ("88888888X",7,"Noelia","12345","spain",5);
INSERT INTO usuarios (id,foto_id,nombre,passwd,procedencia,rol_id) VALUES ("99999999X",10,"Cristina","12345","spain",5);

INSERT INTO experiencias (nombre, descripcion,mins_distancia,usuario_id,mapa)
VALUES ("Museo de cera","museo de cera de barcelona",7,"88888888X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.8605125395225!2d2.174806115818462!3d41.37711517926511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a256e3da7249%3A0x987ab828031eedf0!2sMuseo+de+Cera!5e0!3m2!1ses!2ses!4v1553769273339" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias (nombre, descripcion, hora_inicio,hora_fin,mins_distancia,usuario_id,mapa)
VALUES ("L'ovella negra","taberna nocturna",'20:00:00','03:00:00',5,"99999999X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.9981113844015!2d2.1880684158189125!3d41.395847379263444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a3195c0af997%3A0xe87fb2b71913aad1!2sL&#39;Ovella+Negra+Marina!5e0!3m2!1ses!2ses!4v1553769334565" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias (nombre, descripcion, precio,fecha_inicio,fecha_fin,hora_inicio,hora_fin,mins_distancia,usuario_id,mapa)
VALUES ("Calsotada popular","calsotada popular al parc de la ciutadella",5,'2019-05-24','2019-05-24','11:30:00','19:00:00',20,"66666666X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.3537694311017!2d2.1838265158187506!3d41.38812297926416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2fd439609e1%3A0x42386118e65a3d70!2sParque+de+la+Ciudadela!5e0!3m2!1ses!2ses!4v1553769399732" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias (nombre, descripcion,mins_distancia,usuario_id,mapa)
VALUES ("Cami de les aigues","Ruta per collserola, running, bici, treking",50,"66666666X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.2334770333127!2d2.1171532158193433!3d41.41245017926186!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4983796439809%3A0xd78f794b1ccc878e!2sCarr.+de+las+Aguas%2C+Barcelona!5e0!3m2!1ses!2ses!4v1553769433442" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');

INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("Me ha gustado mucho.","Me lo pase genial es flipante ver como se parecen las figuras a los personajes reales, 100% recomendable",4,1,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("El experiencia es perfecto.","Muy buen ambiente copas, copas no demasiado caras, gran sala llena de billares y futbolines, volvere!",5,2,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No me ha gustado demasiado.","Demasiado ruidoso, mesas sucias donde te quedas pegado, no creo que vuelva",2,2,"11111111X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No esta mal, pero puede mejorar.","El lugar esta bien, pero mejoraria varias cosas",3,2,"77777777X",'2019-03-13');

INSERT INTO ofertas (codigo,nombre,descripcion,fecha_inicio,fecha_fin,hora_inicio,hora_fin,experiencia_id)
VALUES ("#A1S2D3F4","2x1","Dos por uno en copas.",'2019-08-23','2019-08-25','20:00:00','01:00:00',2);
INSERT INTO ofertas (codigo,nombre,descripcion,experiencia_id)
VALUES ("#Z1X2C3V4","descuentos 30%","30% de descuento si presenta este codigo al pagar.",1);
INSERT INTO ofertas (codigo,nombre,descripcion,fecha_inicio,fecha_fin,hora_inicio,hora_fin)
VALUES ("#12345678","10% descuento","10% descuento en desayunos.",'2019-08-23','2019-08-25','08:00:00','10:00:00');
INSERT INTO ofertas (codigo,nombre,descripcion,fecha_inicio,fecha_fin,hora_inicio,hora_fin)
VALUES ("#00000000","15% descuento","15% descuento en comidas.",'2019-08-23','2019-08-25','12:00:00','15:00:00');
INSERT INTO ofertas (codigo,nombre,descripcion,fecha_inicio,fecha_fin,hora_inicio,hora_fin)
VALUES ("#11111111","20% descuento","20% descuento en cenas.",'2019-08-23','2019-08-25','20:00:00','12:00:00');


INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (4,3);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (4,2);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (3,4);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (2,6);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (1,1);

INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (1,11);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (1,12);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (1,13);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (1,14);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (2,15);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (2,16);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (2,17);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (2,18);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (3,19);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (3,20);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (3,21);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (3,22);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (4,23);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (4,24);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (4,25);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (4,26);

INSERT INTO `hotel_developer`.`experiencias`(`descripcion`,`fecha_fin`,`fecha_inicio`,`hora_fin`,`hora_inicio`,`mins_distancia`,`nombre`,`precio`,`usuario_id`,`mapa`)
VALUES("La original “Catedral del siglo XX” de Antonio Gaudí, es el emblema de la ciudad y el monumento más visitado. Su construcción comenzó en 1882 y se espera que finalice en 2026, tan solo 144 años después.",null,null,'09:00:00','18:00:00',5,"Sagrada Familia",15,"66666666X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.637870128193!2d2.1735833221789846!3d41.40367011609628!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2dcd83dfb93%3A0x9bd8aac21bc3c950!2sBas%C3%ADlica+de+la+Sagrada+Fam%C3%ADlia!5e0!3m2!1ses!2ses!4v1553769599333" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (5,1);
INSERT INTO fotos (foto,logo) VALUES ("Sagrada-Familia-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Sagrada-Familia-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Sagrada-Familia-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Sagrada-Familia-3.jpg",false);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (5,27);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (5,28);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (5,29);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (5,30);

INSERT INTO `hotel_developer`.`experiencias`(`descripcion`,`fecha_fin`,`fecha_inicio`,`hora_fin`,`hora_inicio`,`mins_distancia`,`nombre`,`precio`,`usuario_id`,`mapa`)
VALUES("Este inmenso parque fue declarado Patrimonio de la Humanidad por la UNESCO en 1.984, y es otra de las geniales obras de Gaudí, del cual podremos aprender más en su Casa Museo que se encuentra dentro del recinto.",null,null,'08:30:00','18:15:00',5,"Parque Güell",8.50,"66666666X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.1392965394148!2d2.1505058158193804!3d41.41449477926168!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2ae52d441ab%3A0x899a0ba01aaace58!2sPark+G%C3%BCell!5e0!3m2!1ses!2ses!4v1553770111689" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (6,1);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (6,2);
INSERT INTO fotos (foto,logo) VALUES ("Parque-Güell-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Parque-Güell-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Parque-Güell-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Parque-Güell-3.jpg",false);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (6,31);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (6,32);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (6,33);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (6,34);

INSERT INTO `hotel_developer`.`experiencias`(`descripcion`,`fecha_fin`,`fecha_inicio`,`hora_fin`,`hora_inicio`,`mins_distancia`,`nombre`,`precio`,`usuario_id`,`mapa`)
VALUES("Montserrat es un macizo rocoso considerado tradicionalmente la montaña más importante y significativa de Cataluña. Está situada a 30 km del centro de Barcelona entre las comarcas de la Anoia, del Bajo Llobregat y del Bages.",null,null,null,null,90,"Montserrat",0,"66666666X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20074.471054770027!2d1.8213766519025283!3d41.58897947537132!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a45fe93018cfe7%3A0x1ebab017ff86d3c!2sMacizo+de+Montserrat!5e0!3m2!1ses!2ses!4v1553769756844" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (7,1);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (7,2);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (7,3);
INSERT INTO fotos (foto,logo) VALUES ("Montserrat-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Montserrat-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Montserrat-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Montserrat-3.jpg",false);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (7,35);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (7,36);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (7,37);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (7,38);

INSERT INTO `hotel_developer`.`experiencias`(`descripcion`,`fecha_fin`,`fecha_inicio`,`hora_fin`,`hora_inicio`,`mins_distancia`,`nombre`,`precio`,`usuario_id`,`mapa`)
VALUES("Montjuic o Montjuich​​ es una montaña de Barcelona, con una altura de 173 metros sobre el nivel del mar, que alberga un barrio homónimo, en el distrito de Sants-Montjuic.",null,null,null,null,15,"Montjuic",0,"66666666X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11977.856623093407!2d2.158738343771806!3d41.36399931825983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a24230e2aebd%3A0x116c63ebef94b50!2sMontjuic!5e0!3m2!1ses!2ses!4v1553769802657" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (8,1);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (8,2);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (8,3);
INSERT INTO fotos (foto,logo) VALUES ("Montjuic-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Montjuic-1.jpeg",false);
INSERT INTO fotos (foto,logo) VALUES ("Montjuic-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Montjuic-3.jpg",false);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (8,39);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (8,40);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (8,41);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (8,42);

INSERT INTO `hotel_developer`.`experiencias`(`descripcion`,`fecha_fin`,`fecha_inicio`,`hora_fin`,`hora_inicio`,`mins_distancia`,`nombre`,`precio`,`usuario_id`,`mapa`)
VALUES("La Festa Major de Gràcia 2018 tiene actividades culturales, creativas, gastronómicas  y musicales durante toda la semana. También hay concursos de baile y deportivos, espectáculos de castellers, teatro, exposiciones y cine al aire libre.","2019-08-15","2019-08-21",null,null,15,"Festa Major de Gràcia",0,"88888888X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11970.669083146506!2d2.1485696437874817!3d41.40303171820245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2bda822050f%3A0xb4d1fd36e015a923!2sVila+de+Gr%C3%A0cia%2C+Barcelona!5e0!3m2!1ses!2ses!4v1553769839813" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (9,1);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (9,4);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (9,5);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (9,6);
INSERT INTO fotos (foto,logo) VALUES ("Festa-Major-de-Gràcia-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Festa-Major-de-Gràcia-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Festa-Major-de-Gràcia-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Festa-Major-de-Gràcia-3.jpg",false);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (9,43);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (9,44);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (9,45);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (9,46);

INSERT INTO `hotel_developer`.`experiencias`(`descripcion`,`fecha_fin`,`fecha_inicio`,`hora_fin`,`hora_inicio`,`mins_distancia`,`nombre`,`precio`,`usuario_id`,`mapa`)
VALUES("Las Fiestas de Sants o Fiesta Mayor de Sants (en catalán, Festa Major de Sants) se celebra en el barrio de Sants (Barcelona) el día de san Bartolomé (24 de agosto). Su elemento más distintivo es la ornamentación de calles, pero también se celebran conciertos, bailes populares, exhibiciones castelleras, gigantes y cabezudos, correfocs de diablos y dragones de fuego y otros eventos festivos","2019-08-24","2019-08-30",null,null,15,"Fiesta Mayor de Sants",0,"88888888X",'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11975.376755397396!2d2.1366403234772666!3d41.37746981039048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a498863a0819fd%3A0xfdb69df66dad168c!2sSants%2C+Barcelona!5e0!3m2!1ses!2ses!4v1553769881113" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>');
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (10,1);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (10,4);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (10,5);
INSERT INTO experiencias_tipos (experiencia_id,tipos_id) VALUES (10,6);
INSERT INTO fotos (foto,logo) VALUES ("Fiesta-Mayor-de-Sants-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Fiesta-Mayor-de-Sants-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Fiesta-Mayor-de-Sants-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Fiesta-Mayor-de-Sants-3.jpg",false);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (10,47);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (10,48);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (10,49);
INSERT INTO experiencias_fotos (experiencia_id,fotos_id) VALUES (10,50);

INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("El sitio es perfecto.","El sitio es perfecto",5,5,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No me ha gustado.","El sitio no es de mi agrado",1,5,"11111111X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No esta mal, pero puede mejorar.","El lugar esta bien, pero mejoraria varias cosas",3,5,"77777777X",'2019-03-13');

INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("El sitio es perfecto.","El sitio es perfecto",5,6,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No me ha gustado.","El sitio no es de mi agrado",1,6,"11111111X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No esta mal, pero puede mejorar.","El lugar esta bien, pero mejoraria varias cosas",3,6,"77777777X",'2019-03-13');

INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("El sitio es perfecto.","El sitio es perfecto",5,7,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No me ha gustado.","El sitio no es de mi agrado",1,7,"11111111X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No esta mal, pero puede mejorar.","El lugar esta bien, pero mejoraria varias cosas",3,7,"77777777X",'2019-03-13');

INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("El sitio es perfecto.","El sitio es perfecto",5,8,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No me ha gustado.","El sitio no es de mi agrado",1,8,"11111111X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No esta mal, pero puede mejorar.","El lugar esta bien, pero mejoraria varias cosas",3,8,"77777777X",'2019-03-13');

INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("El sitio es perfecto.","El sitio es perfecto",5,9,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No me ha gustado.","El sitio no es de mi agrado",1,9,"11111111X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No esta mal, pero puede mejorar.","El lugar esta bien, pero mejoraria varias cosas",3,9,"77777777X",'2019-03-13');

INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("El sitio es perfecto.","El sitio es perfecto",5,10,"00000000X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No me ha gustado.","El sitio no es de mi agrado",1,10,"11111111X",'2019-03-13');
INSERT INTO comentarios (titulo,comentario,puntos,experiencia_id,usuario_id,fecha) 
VALUES ("No esta mal, pero puede mejorar.","El lugar esta bien, pero mejoraria varias cosas",3,10,"77777777X",'2019-03-13');

INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(1,1);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(1,2);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(1,3);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(1,4);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(1,6);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(2,1);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(2,2);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(2,3);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(2,4);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(2,5);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(2,6);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(3,2);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(3,4);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(3,5);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(3,6);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(4,1);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(4,2);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(4,3);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(4,4);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(5,1);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(5,2);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(5,4);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(5,5);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(6,1);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(6,2);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(6,4);
INSERT INTO perfiles_tipos (perfil_id,tipos_id) VALUES(6,5);

/*insercion de fotos de servicios 29/03/2019*/
INSERT INTO fotos (foto,logo) VALUES ("Piscina-exterior-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Piscina-exterior-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Piscina-exterior-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Piscina-exterior-3.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-SPA-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-SPA-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-SPA-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-SPA-3.jpeg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-desayunos-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-desayunos-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-desayunos-2.jpeg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-desayunos-3.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-comidas-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-comidas-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-comidas-2.jpeg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-comidas-3.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-cenas-0.jpg",true);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-cenas-1.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-cenas-2.jpg",false);
INSERT INTO fotos (foto,logo) VALUES ("Servicio-cenas-3.jpg",false);

/*servicio_id fotos_id 29/03/2019*/
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (1,51);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (1,52);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (1,53);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (1,54);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (2,55);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (2,56);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (2,57);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (2,58);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (3,59);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (3,60);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (3,61);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (3,62);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (4,63);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (4,64);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (4,65);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (4,66);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (5,67);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (5,68);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (5,69);
INSERT INTO servicios_fotos (servicio_id,fotos_id) VALUES (5,70);




COMMIT;