CREATE USER 'developer'@'localhost' IDENTIFIED BY 'discoverme';
CREATE USER 'developer'@'%' IDENTIFIED BY 'discoverme';
GRANT ALL PRIVILEGES ON * . * TO 'developer'@'localhost';
GRANT ALL PRIVILEGES ON * . * TO 'developer'@'%';
FLUSH PRIVILEGES;
