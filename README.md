# Instalador Discoverme

instalador para desplegar la aplicacion web DiscoverMe en contenedores docker.

## contenedores:
- payara/micro:latest con el fichero DiscoverMe.war ejecutado dentro https://ip-maquina:8181/DiscoverMe
- mysql:latest con la base de datos cargada al iniciarse 
- adminer:latest para gestionar la base de datos via web (http://ip-maquina:7979)

## requisitos:
- docker-ce
- docker-compose

## instalacion:
1. instalar docker-ce https://docs.docker.com/install/linux/docker-ce/ubuntu/
2. instalar docker-compose https://docs.docker.com/compose/install/
3. descargar el repositorio 
4. bash discoverme.sh -i 
7. abrir un navegador y poner https://ip-de-la-maquina:8181/DiscoverMe

## actualización del fichero DiscoverMe.war
1. copiar el nuevo fichero war en la carpeta ~/instalador-discoverme/payara-container/
2. bash discoverme -u

## desinstalacion:
1. sudo bash discoverme.sh -r (IMPORTANTE: usar 'sudo', para eliminar correctamente las carpetas generadas por docker)

### varios:

NOTA: el script sobreescribe el fichero jdbc.properties para configurar el fichero con la ip local del contenedor mysql



