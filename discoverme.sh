#!/bin/bash
#discoverme.sh
#variables
version=0.1
#comprobar requisitos (docker/docker-compose)



#numero de parametros
if [ $# -ne 1 ]; then
	clear
	echo "discoverme v$version"
	echo ""
	echo "[ERROR: se esperaba un parametro]"
	echo "Ejemplo: $0 -h "
	exit 1
fi





###############################################################

#funciones que comprueben requisitos
function check_docker(){
    systemctl status docker > /dev/null
	if [ $? -ne 0 ]; then
	clear
	echo "discoverme v$version"
	echo ""
	echo "[ERROR: se necesita docker-ce para ejecutar el script]"
	echo "https://docs.docker.com/install/"
	exit 1
	fi
}

function check_docker-compose(){
    which docker-compose > /dev/null
	if [ $? -ne 0 ]; then
	clear
	echo "discoverme v$version"
	echo ""
	echo "[ERROR: se necesita docker-compose para ejecutar el script]"
	echo "https://docs.docker.com/compose/install/"
	exit 1
	fi
}
#funcion install
function install(){
    echo "[INSTALANDO Discoverme..]"
    chmod 777 payara-container/domain.xml
    docker network create testNetwork
    cd docker-compose
    docker-compose up -d
    clear
    docker-compose ps
    echo "Instalacion completada. [OK]"
}

#funcion remove
function remove(){
    echo "[DESINSTALANDO Discoverme..]"
    #sed -i 's/Listen 333/Listen 80/' apache-container/config/httpd.conf
    cd docker-compose
    docker-compose down
    cd ..
    cd mysql-container
    rm -R mysql-data 
    mkdir mysql-data 
    chown $USER:$USER -R mysql-data 
    docker rmi payaratuneado
    docker rmi adminer
    docker rmi mysql
    #docker rmi alpine
    #docker rmi nimmis/alpine-apache
    #docker rmi squidfunk/mkdocs-material
    #docker rmi jwilder/nginx-proxy
    docker network rm testNetwork
    #docker network rm nginx-proxy
    clear
    docker ps -a
    echo "Aplicacion desinstalada. [OK]"
}

#funcion parametro_incorrecto
function parametro_incorrecto(){
    clear
    echo "discoverme v$version"
    echo ""
    echo "[ERROR: parametro incorrecto.]"
    echo "$0 -h para mas informacion."
    exit 1
}

#funcion help
function help() {
    clear
    echo "[AYUDA]"
    echo "instalador discoverme v$version"
    echo ""
    echo "discoverme.sh [opcion]"
    echo "opciones:"
    echo "	-h ayuda."
    echo "	-i instala discoverme."
    echo "	-r borra discoverme. [para desinstalar usar sudo]"
    echo "	-u actualiza la ip y el fichero war."
    echo ""
    echo "26/04/2019"
    echo ""
}

#funcion modificaIpWar
function modifica_ips(){
    echo "recolectando ip..."
    ip -o addr show up primary scope global | grep -o 'inet6\? [0-9a-f\.:]*' | cut -f 2 -d ' '>/tmp/testip.txt
    head -1 /tmp/testip.txt|tail -1 >/tmp/ip.txt
    rm /tmp/testip.txt
    ip=$(cat /tmp/ip.txt)
    echo "$ip"
    rm /tmp/ip.txt
    echo "[OK]"
    echo "Modificando fichero  war..."
    unzip payara-container/DiscoverMe.war -d /tmp/test
    sed -i -e 's/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}/'$ip'/g' /tmp/test/WEB-INF/classes/jdbc.properties
    cd /tmp/test/
    zip -r DiscoverMe.war *
    chmod 775 /tmp/test/DiscoverMe.war
    chown $USER:$USER /tmp/test/DiscoverMe.war
    mv /tmp/test/DiscoverMe.war /home/$USER/tests/instalador-discoverme/payara-container/
    cd ~/tests/instalador-discoverme
    rm -R /tmp/test
    echo "[OK]"
    #echo "Modificando index.html del contenedor apache"
    #sed -i -e 's/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}/'$ip'/g' apache-container/html/index.html
    #echo "[OK]"
    #echo "Modificando puerto de escucha apache"
    #sed -i 's/Listen 80/Listen 333/' apache-container/config/httpd.conf
    #echo "[OK]"
}


###############################################################


case "$1" in
    
    #instala discoverme
    -i)
    	check_docker
	check_docker-compose
        modifica_ips
    	install
    ;;

    #desinstala discoverme
    -r)
    	check_docker
	check_docker-compose
    	remove
    ;;

    #muestra ayuda
    -h)
        help
    ;;

    #edita war modificado y renicia contenedor payara
    -u)
        modificaIpWar
        docker cp payara-container/DiscoverMe.war payara_server:/opt/payara/
        docker restart payara_server
    ;;


    #opcion invalida
    *)
        parametro_incorrecto
    
    ;;
esac
exit 0



